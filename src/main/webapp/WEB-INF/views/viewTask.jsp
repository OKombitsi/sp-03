<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11.02.2020
  Time: 13:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <title>TASK VIEW</title>
</head>
<body>
<table class="table table-dark">
    <tr>
        <th style="width: 7%; text-align: center">MAIN</th>
        <th style="width: 7%; text-align: center"><a href="/projectList">PROJECTS</a></th>
        <th style="width: 7%; text-align: center"><a href="/taskList">TASKS</a></th>
        <th/>
    </tr>
</table>

<h2>TASK VIEW</h2>

<table class="table table-bordered">
    <table width="100%" border="1" rules="all">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">NAME</th>
            <th scope="col">DESCRIPTION</th>
            <th scope="col">DATE ADD</th>
            <th scope="col">DATE START</th>
            <th scope="col">DATE FINISH</th>
            <th scope="col">STATUS</th>
            <th scope="col">PROJECT ID</th>
        </tr>
        <tr>
            <td>${task.id}</td>
            <td>${task.name}</td>
            <td>${task.description}</td>
            <td>${task.dateAdd}</td>
            <td>${task.dateStart}</td>
            <td>${task.dateFinish}</td>
            <td>${task.displayName}</td>
            <td>${task.projectId}</td>
        </tr>
        </tbody>
    </table>
</table>
</body>
</html>