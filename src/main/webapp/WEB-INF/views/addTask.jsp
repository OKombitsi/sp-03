<%--
  Created by IntelliJ IDEA.
  User: okombitsi
  Date: 12/02/2020
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <title>ADD TASK</title>
</head>
<body>

<c:url value="/addTask" var="var"/>
<form action="${var}" method="POST">
    <table>
        <tr>
            <th>
                <label for="name">PROJECT ID</label>
            </th>
        </tr>
        <td>
            <input type="text" name="projectId" id="projectId">
        </td>

        <tr>
            <th>
                <label for="name">NAME</label>
            </th>
        </tr>
        <td>
            <input type="text" name="name" id="name">
        </td>
        <tr>
            <th>
                <label for="description">DESCRIPTION</label>
            </th>
        </tr>

        <td>
            <input type="text" name="description" id="description">
        </td>
        <br>
        <br>
        <tr>
            <th>
                <input class="btn btn-secondary btn-lg active" role="button" aria-pressed="true" type="submit"
                       value="Create task">
            </th>
        </tr>
    </table>
</form>
</body>
</html>
