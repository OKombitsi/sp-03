<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11.02.2020
  Time: 13:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <title>PROJECT MANAGEMENT</title>
</head>
<body>
<table class="table table-dark">
    <tr>
        <th style="width: 7%; text-align: center">MAIN</th>
        <th style="width: 7%; text-align: center"><a href="/projectList">PROJECTS</a></th>
        <th style="width: 7%; text-align: center"><a href="/taskList">TASKS</a></th>
        <th/>
    </tr>
</table>

<h2>PROJECTS</h2>

<table class="table table-bordered">
    <table width="100%" border="1" rules="all">
        <tr>
            <th scope="col">№</th>
            <th scope="col">ID</th>
            <th scope="col">NAME</th>
            <th scope="col">DESCRIPTION</th>
            <th scope="col">VIEW</th>
            <th scope="col">EDIT</th>
            <th scope="col">REMOVE</th>
        </tr>
        <c:set var="count" value="0" scope="page"/>
        <c:forEach var="project" items="${projectList}">
            <c:set var="count" value="${count + 1}" scope="page"/>
            <tr>
                <th style="text-align: center">${count}</th>
                <td>${project.id}</td>
                <td>${project.name}</td>
                <td>${project.description}</td>
                <td style="text-align: center"><a href="/viewProject/${project.id}">VIEW</a></td>
                <td style="text-align: center"><a href="/editProject/${project.id}">EDIT</a></td>
                <td style="text-align: center"><a href="/deleteProject/${project.id}">DELETE</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</table>
<br>
<a href="/addProject" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">CREATE PROJECT</a>
</body>
</html>