<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11.02.2020
  Time: 17:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <title>EDIT TASK</title>
</head>
<body>

<c:url value="/editTask" var="var"/>
<form action="${var}" method="POST">
    <input type="hidden" name="id" value="${task.id}">
    <table>
        <tr>
            <th>
                <label for="name">PROJECT ID</label>
            </th>
        </tr>
        <td>
            <input type="text" name="projectId" id="projectId">
        </td>

        <tr>
            <th>
                <label for="name">NAME</label>
            </th>
        </tr>
        <td>
            <input type="text" name="name" id="name">
        </td>
        <tr>
            <th>
                <label for="description">DESCRIPTION</label>
            </th>
        </tr>

        <td>
            <input type="text" name="description" id="description">
        </td>
        <tr>
            <th>
                <label for="dateAdd">Date ADD</label>
            </th>
        </tr>
        <td>
            <fmt:formatDate var="fmtDate" value="${task.dateAdd}" pattern="yyyy-MM-dd"/>
            <input type="date" value="${fmtDate}" name="dateAdd" id="dateAdd">
        </td>
        <tr>
            <th>
                <label for="dateStart">Date START</label>
            </th>
        </tr>
        <td>
            <fmt:formatDate var="fmtStartDate" value="${task.dateStart}" pattern="yyyy-MM-dd"/>
            <input type="date" value="${fmtStartDate}" name="dateStart" id="dateStart">
        </td>
        <tr>
            <th>
                <label for="dateFinish">Date FINISH</label>
            </th>
        </tr>
        <td>
            <fmt:formatDate var="fmtFinishDate" value="${task.dateFinish}" pattern="yyyy-MM-dd"/>
            <input type="date" value="${fmtFinishDate}" name="dateFinish" id="dateFinish">
        </td>
        <tr>
            <th>
                <label for="displayName">STATUS</label>
            </th>
        </tr>
        <tr>
            <td>
                <p><select name="displayName" id="displayName" style="width: 100%">
                    <option>select one type</option>
                    <option>SCHEDULE</option>
                    <option>PROGRESS</option>
                    <option>DONE</option>
                </select></p>
            </td>
        </tr>

        <br>
        <br>
        <tr>
            <th>
                <input type="submit" value="Edit task">
            </th>
        </tr>
    </table>
</form>
</body>
</html>