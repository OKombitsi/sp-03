<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11.02.2020
  Time: 13:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <title>PROJECT VIEW</title>
</head>
<body>
<table class="table table-dark">
    <tr>
        <th style="width: 7%; text-align: center">MAIN</th>
        <th style="width: 7%; text-align: center"><a href="/projectList">PROJECTS</a></th>
        <th style="width: 7%; text-align: center"><a href="/taskList">TASKS</a></th>
        <th/>
    </tr>
</table>

<h2>PROJECT VIEW</h2>

<table class="table table-bordered">
    <table width="100%" border="1" rules="all">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">NAME</th>
            <th scope="col">DESCRIPTION</th>
            <th scope="col">DATE ADD</th>
            <th scope="col">DATE START</th>
            <th scope="col">DATE FINISH</th>
            <th scope="col">STATUS</th>
        </tr>
        <tr>
            <td>${project.id}</td>
            <td>${project.name}</td>
            <td>${project.description}</td>
            <td>${project.dateAdd}</td>
            <td>${project.dateStart}</td>
            <td>${project.dateFinish}</td>
            <td>${project.displayName}</td>
        </tr>
        </tbody>
    </table>
</table>
</body>
</html>