<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 11.02.2020
  Time: 17:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<html>
<head>
    <title>ADD Project</title>
</head>
<body>

<c:url value="/addProject" var="var"/>
<form action="${var}" method="POST">
    <table>
        <tr>
            <th>
                <label for="name">NAME</label>
            </th>
        </tr>
        <td>
            <input type="text" name="name" id="name">
        </td>
        <tr>
            <th>
                <label for="description">DESCRIPTION</label>
            </th>
        </tr>

        <td>
            <input type="text" name="description" id="description">
        </td>
        <br>
        <br>
        <tr>
            <th>
                <input class="btn btn-secondary btn-lg active" role="button" aria-pressed="true" type="submit"
                       value="Create project">
            </th>
        </tr>
    </table>
</form>
</body>
</html>