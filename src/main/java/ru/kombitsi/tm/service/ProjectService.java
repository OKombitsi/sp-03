package ru.kombitsi.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kombitsi.tm.dto.ProjectDto;
import ru.kombitsi.tm.repository.ProjectRepository;
import ru.kombitsi.tm.util.DtoConvert;

import java.util.List;

@Getter
@Setter
@Service
public final class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Nullable
    public List<ProjectDto> listAllProject() {
        return DtoConvert.ProjectListToDtoList(projectRepository.findAll());
    }

    public ProjectDto updateProject(@NotNull ProjectDto project) {
        if (project == null) return null;
        return DtoConvert.ProjectToDTO(projectRepository.saveAndFlush(DtoConvert.DTOToProject(project)));
    }

    @Nullable
    public ProjectDto findOneProjectById(@NotNull String id) {
        if (id.isEmpty() || id == null) return null;
        return DtoConvert.ProjectToDTO(projectRepository.getOne(id));
    }

    public void removeOneProject(@NotNull ProjectDto projectDto) {
        if (projectDto == null) return;
        projectRepository.delete(DtoConvert.DTOToProject(projectDto));
    }
}

