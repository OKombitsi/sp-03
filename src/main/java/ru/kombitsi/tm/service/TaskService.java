package ru.kombitsi.tm.service;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kombitsi.tm.dto.TaskDto;
import ru.kombitsi.tm.repository.TaskRepository;
import ru.kombitsi.tm.util.DtoConvert;

import java.util.List;

@Getter
@Setter
@Service
@NoArgsConstructor
public final class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Nullable
    public List<TaskDto> showAllTaskByAllUsers() {
        return DtoConvert.TaskListToDtoList(taskRepository.findAll());
    }

    @Nullable
    public TaskDto getTaskById(@NotNull String id) throws Exception {
        if (id == null) return null;
        return DtoConvert.TaskToDto(taskRepository.getOne(id));
    }

    @Nullable
    public TaskDto updateTask(@NotNull TaskDto taskDto) {
        if (taskDto == null) return null;
        return DtoConvert.TaskToDto(taskRepository.save(DtoConvert.DTOToTask(taskDto)));
    }

    public void removeOneTask(TaskDto taskDto) {
        if (taskDto == null) return;
        taskRepository.delete(DtoConvert.DTOToTask(taskDto));
    }
}

