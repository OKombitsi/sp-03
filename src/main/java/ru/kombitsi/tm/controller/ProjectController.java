package ru.kombitsi.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.kombitsi.tm.dto.ProjectDto;
import ru.kombitsi.tm.service.ProjectService;

import java.util.List;

@Controller
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/projectList", method = RequestMethod.GET)
    public ModelAndView allProjects() {
        List<ProjectDto> projectList = projectService.listAllProject();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("projectList");
        modelAndView.addObject("projectList", projectList);
        return modelAndView;
    }

    @RequestMapping(value = "/editProject/{id}", method = RequestMethod.GET)
    public ModelAndView editProject(@PathVariable("id") String id) {
        ProjectDto project = projectService.findOneProjectById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("editProject");
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @RequestMapping(value = "/editProject", method = RequestMethod.POST)
    public ModelAndView editProject(@ModelAttribute("project") ProjectDto project) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/projectList");
        projectService.updateProject(project);
        return modelAndView;
    }

    @RequestMapping(value = "/viewProject/{id}", method = RequestMethod.GET)
    public ModelAndView viewProject(@PathVariable("id") String id) {
        ProjectDto project = projectService.findOneProjectById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("viewProject");
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @RequestMapping(value = "/deleteProject/{id}", method = RequestMethod.GET)
    public ModelAndView deleteProject(@PathVariable("id") String id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/projectList");
        ProjectDto project = projectService.findOneProjectById(id);
        projectService.removeOneProject(project);
        return modelAndView;
    }

    @RequestMapping(value = "/addProject", method = RequestMethod.GET)
    public ModelAndView addProject() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("addProject");
        return modelAndView;
    }

    @RequestMapping(value = "/addProject", method = RequestMethod.POST)
    public ModelAndView addProject(@ModelAttribute("project") ProjectDto project) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/projectList");
        projectService.updateProject(project);
        return modelAndView;
    }
}
