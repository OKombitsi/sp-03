package ru.kombitsi.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.kombitsi.tm.dto.TaskDto;
import ru.kombitsi.tm.service.TaskService;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/taskList", method = RequestMethod.GET)
    public ModelAndView allTasks() {
        List<TaskDto> taskList = taskService.showAllTaskByAllUsers();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("taskList");
        modelAndView.addObject("taskList", taskList);
        return modelAndView;
    }

    @RequestMapping(value = "/editTask/{id}", method = RequestMethod.GET)
    public ModelAndView editTask(@PathVariable("id") String id) throws Exception {
        TaskDto task = taskService.getTaskById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("editTask");
        modelAndView.addObject("task", task);
        return modelAndView;
    }

    @RequestMapping(value = "/editTask", method = RequestMethod.POST)
    public ModelAndView editTask(@ModelAttribute("task") TaskDto task) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/taskList");
        taskService.updateTask(task);
        return modelAndView;
    }

    @RequestMapping(value = "/removeTask", method = RequestMethod.GET)
    public ModelAndView removeTask(TaskDto task) {
        ModelAndView modelAndView = new ModelAndView();
        taskService.removeOneTask(task);
        modelAndView.setViewName("removeTask");
        return modelAndView;
    }

    @RequestMapping(value = "/addTask", method = RequestMethod.GET)
    public ModelAndView addProject() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("addTask");
        return modelAndView;
    }

    @RequestMapping(value = "/addTask", method = RequestMethod.POST)
    public ModelAndView addTask(@ModelAttribute("task") TaskDto task) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/taskList");
        taskService.updateTask(task);
        return modelAndView;
    }

    @RequestMapping(value = "/deleteTask/{id}", method = RequestMethod.GET)
    public ModelAndView deleteTask(@PathVariable("id") String id) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/taskList");
        TaskDto task = taskService.getTaskById(id);
        taskService.removeOneTask(task);
        return modelAndView;
    }

    @RequestMapping(value = "/viewTask/{id}", method = RequestMethod.GET)
    public ModelAndView viewTask(@PathVariable("id") String id) throws Exception {
        TaskDto task = taskService.getTaskById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("viewTask");
        modelAndView.addObject("task", task);
        return modelAndView;
    }
}
